// Code generated; Do not regenerate the overwrite after editing.

package account

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// AccountWithID is Account with ID
type AccountWithID struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"account_id"`
	Account    `bson:",inline"`
	CreateTime time.Time `bson:"create_time,omitempty" json:"create_time"`
	UpdateTime time.Time `bson:"update_time,omitempty" json:"update_time"`
}

// AccountRecord is record of the Account
type AccountRecord struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"account_record_id"`
	AccountID   bson.ObjectId `bson:"account_id,omitempty" json:"account_id"`
	Recent      *Account      `bson:"recent,omitempty" json:"recent"`
	Current     *Account      `bson:"current,omitempty" json:"current"`
	RecentTime  time.Time     `bson:"recent_time,omitempty" json:"recent_time"`
	CurrentTime time.Time     `bson:"current_time,omitempty" json:"current_time"`
	Times       int           `bson:"times,omitempty" json:"times"`
}

// AccountService is service of the Account
// #path:"/account/"#
type AccountService struct {
	db       *mgo.Collection
	dbRecord *mgo.Collection
}

// NewAccountService Create a new AccountService
func NewAccountService(db *mgo.Collection) (*AccountService, error) {
	dbRecord := db.Database.C(db.Name + "_record")
	dbRecord.EnsureIndex(mgo.Index{Key: []string{"account_id"}})
	return &AccountService{
		db:       db,
		dbRecord: dbRecord,
	}, nil
}

// Create a Account
// #route:"POST /"#
func (s *AccountService) Create(account *Account) (accountID bson.ObjectId /* #name:"account_id"# */, err error) {
	accountID = bson.NewObjectId()
	now := bson.Now()
	err = s.db.Insert(&AccountWithID{
		ID:         accountID,
		Account:    *account,
		CreateTime: now,
		UpdateTime: now,
	})
	if err != nil {
		return "", err
	}
	return accountID, nil
}

// Update the Account
// #route:"PUT /{account_id}"#
func (s *AccountService) Update(accountID bson.ObjectId /* #name:"account_id"# */, account *Account) (err error) {
	recent, err := s.Get(accountID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.UpdateId(accountID, bson.D{{"$set", &AccountWithID{
		Account:    *account,
		UpdateTime: bson.Now(),
	}}})
	if err != nil {
		return err
	}

	current, err := s.Get(accountID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.record(recent, &current.Account)
	if err != nil {
		return err
	}

	return nil
}

// Delete the Account
// #route:"DELETE /{account_id}"#
func (s *AccountService) Delete(accountID bson.ObjectId /* #name:"account_id"# */) (err error) {
	recent, err := s.Get(accountID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.RemoveId(accountID)
	if err != nil {
		return err
	}

	err = s.record(recent, nil)
	if err != nil {
		return err
	}

	return nil
}

// Get the Account
// #route:"GET /{account_id}"#
func (s *AccountService) Get(accountID bson.ObjectId /* #name:"account_id"# */) (account *AccountWithID, err error) {
	q := s.db.FindId(accountID)
	err = q.One(&account)
	if err != nil {
		return nil, err
	}
	return account, nil
}

// List of the Account
// #route:"GET /"#
func (s *AccountService) List(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */, offset, limit int, sort string) (accounts []*AccountWithID, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m).Skip(offset).Limit(limit).Sort(sort)
	err = q.All(&accounts)
	if err != nil {
		return nil, err
	}
	return accounts, nil
}

// Count of the Account
// #route:"GET /count"#
func (s *AccountService) Count(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */) (count int, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m)
	return q.Count()
}

// RecordList of the Account record list
// #route:"GET /{account_id}/record"#
func (s *AccountService) RecordList(accountID bson.ObjectId /* #name:"account_id"# */, offset, limit int) (accountRecords []*AccountRecord, err error) {
	m := bson.D{{"account_id", accountID}}
	q := s.dbRecord.Find(m).Skip(offset).Limit(limit)
	err = q.All(&accountRecords)
	if err != nil {
		return nil, err
	}
	return accountRecords, nil
}

// RecordCount of the Account record count
// #route:"GET /{account_id}/record/count"#
func (s *AccountService) RecordCount(accountID bson.ObjectId /* #name:"account_id"# */) (count int, err error) {
	m := bson.D{{"account_id", accountID}}
	q := s.dbRecord.Find(m)
	return q.Count()
}

func (s *AccountService) record(account *AccountWithID, current *Account) error {
	if account == nil {
		return nil
	}
	count, err := s.dbRecord.Find(bson.D{{"account_id", account.ID}}).Count()
	if err != nil {
		return err
	}
	record := &AccountRecord{
		AccountID:   account.ID,
		Current:     current,
		CurrentTime: bson.Now(),
		Times:       count + 1,
		Recent:      &account.Account,
		RecentTime:  account.UpdateTime,
	}
	return s.dbRecord.Insert(record)
}
