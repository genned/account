package oauth

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type OAuth struct {
	AccountID   bson.ObjectId `bson:"account_id" json:"account_id"`
	Platform    string        `bson:"platform" json:"platform"`
	AccessID    string        `bson:"access_id" json:"access_id"`
	AccessToken string        `bson:"access_token" json:"access_token"`
	Expires     time.Time     `bson:"expires" json:"expires"`
}
