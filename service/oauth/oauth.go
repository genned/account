// Code generated; Do not regenerate the overwrite after editing.

package oauth

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// OAuthWithID is OAuth with ID
type OAuthWithID struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"oauth_id"`
	OAuth      `bson:",inline"`
	CreateTime time.Time `bson:"create_time,omitempty" json:"create_time"`
	UpdateTime time.Time `bson:"update_time,omitempty" json:"update_time"`
}

// OAuthRecord is record of the OAuth
type OAuthRecord struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"oauth_record_id"`
	OAuthID     bson.ObjectId `bson:"oauth_id,omitempty" json:"oauth_id"`
	Recent      *OAuth        `bson:"recent,omitempty" json:"recent"`
	Current     *OAuth        `bson:"current,omitempty" json:"current"`
	RecentTime  time.Time     `bson:"recent_time,omitempty" json:"recent_time"`
	CurrentTime time.Time     `bson:"current_time,omitempty" json:"current_time"`
	Times       int           `bson:"times,omitempty" json:"times"`
}

// OAuthService is service of the OAuth
// #path:"/oauth/"#
type OAuthService struct {
	db       *mgo.Collection
	dbRecord *mgo.Collection
}

// NewOAuthService Create a new OAuthService
func NewOAuthService(db *mgo.Collection) (*OAuthService, error) {
	dbRecord := db.Database.C(db.Name + "_record")
	dbRecord.EnsureIndex(mgo.Index{Key: []string{"oauth_id"}})
	return &OAuthService{
		db:       db,
		dbRecord: dbRecord,
	}, nil
}

// Create a OAuth
// #route:"POST /"#
func (s *OAuthService) Create(oauth *OAuth) (oauthID bson.ObjectId /* #name:"oauth_id"# */, err error) {
	oauthID = bson.NewObjectId()
	now := bson.Now()
	err = s.db.Insert(&OAuthWithID{
		ID:         oauthID,
		OAuth:      *oauth,
		CreateTime: now,
		UpdateTime: now,
	})
	if err != nil {
		return "", err
	}
	return oauthID, nil
}

// Update the OAuth
// #route:"PUT /{oauth_id}"#
func (s *OAuthService) Update(oauthID bson.ObjectId /* #name:"oauth_id"# */, oauth *OAuth) (err error) {
	recent, err := s.Get(oauthID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.UpdateId(oauthID, bson.D{{"$set", &OAuthWithID{
		OAuth:      *oauth,
		UpdateTime: bson.Now(),
	}}})
	if err != nil {
		return err
	}

	current, err := s.Get(oauthID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.record(recent, &current.OAuth)
	if err != nil {
		return err
	}

	return nil
}

// Delete the OAuth
// #route:"DELETE /{oauth_id}"#
func (s *OAuthService) Delete(oauthID bson.ObjectId /* #name:"oauth_id"# */) (err error) {
	recent, err := s.Get(oauthID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.RemoveId(oauthID)
	if err != nil {
		return err
	}

	err = s.record(recent, nil)
	if err != nil {
		return err
	}

	return nil
}

// Get the OAuth
// #route:"GET /{oauth_id}"#
func (s *OAuthService) Get(oauthID bson.ObjectId /* #name:"oauth_id"# */) (oauth *OAuthWithID, err error) {
	q := s.db.FindId(oauthID)
	err = q.One(&oauth)
	if err != nil {
		return nil, err
	}
	return oauth, nil
}

// List of the OAuth
// #route:"GET /"#
func (s *OAuthService) List(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */, offset, limit int, sort string) (oauths []*OAuthWithID, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m).Skip(offset).Limit(limit).Sort(sort)
	err = q.All(&oauths)
	if err != nil {
		return nil, err
	}
	return oauths, nil
}

// Count of the OAuth
// #route:"GET /count"#
func (s *OAuthService) Count(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */) (count int, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m)
	return q.Count()
}

// RecordList of the OAuth record list
// #route:"GET /{oauth_id}/record"#
func (s *OAuthService) RecordList(oauthID bson.ObjectId /* #name:"oauth_id"# */, offset, limit int) (oauthRecords []*OAuthRecord, err error) {
	m := bson.D{{"oauth_id", oauthID}}
	q := s.dbRecord.Find(m).Skip(offset).Limit(limit)
	err = q.All(&oauthRecords)
	if err != nil {
		return nil, err
	}
	return oauthRecords, nil
}

// RecordCount of the OAuth record count
// #route:"GET /{oauth_id}/record/count"#
func (s *OAuthService) RecordCount(oauthID bson.ObjectId /* #name:"oauth_id"# */) (count int, err error) {
	m := bson.D{{"oauth_id", oauthID}}
	q := s.dbRecord.Find(m)
	return q.Count()
}

func (s *OAuthService) record(oauth *OAuthWithID, current *OAuth) error {
	if oauth == nil {
		return nil
	}
	count, err := s.dbRecord.Find(bson.D{{"oauth_id", oauth.ID}}).Count()
	if err != nil {
		return err
	}
	record := &OAuthRecord{
		OAuthID:     oauth.ID,
		Current:     current,
		CurrentTime: bson.Now(),
		Times:       count + 1,
		Recent:      &oauth.OAuth,
		RecentTime:  oauth.UpdateTime,
	}
	return s.dbRecord.Insert(record)
}
