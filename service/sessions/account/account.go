package user

import (
	"net/http"

	"gitlab.com/genned/account/service/account"
	"gitlab.com/genned/account/service/sessions"
)

// Account #middleware:"account"#
func Account(r *http.Request) (accountWithID *account.AccountWithID, err error) {
	ctx := r.Context()
	accountI := ctx.Value(sessions.KeyAccount)
	accountWithID, _ = accountI.(*account.AccountWithID)
	return accountWithID, nil
}
