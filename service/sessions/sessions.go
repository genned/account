package sessions

import (
	"context"
	"encoding/gob"
	"fmt"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/gorilla/sessions"
	"github.com/kidstuff/mongostore"
	"gitlab.com/genned/account/service/account"
	"gitlab.com/genned/account/service/auth"
	"gitlab.com/genned/account/service/oauth"
	"gitlab.com/genned/account/service/password"
)

func init() {
	gob.Register(&account.AccountWithID{})
}

// SessionsService #path:"/sessions/"#
type SessionsService struct {
	name     string
	store    *mongostore.MongoStore
	account  *account.AccountService
	auth     *auth.AuthService
	oauth    *oauth.OAuthService
	password *password.PasswordService
}

func NewSessionsService(name string, db *mgo.Collection, account *account.AccountService, auth *auth.AuthService, oauth *oauth.OAuthService, password *password.PasswordService) (*SessionsService, error) {
	store := mongostore.NewMongoStore(db, 0, true, []byte("genned/account"), []byte(name))
	return &SessionsService{name, store, account, auth, oauth, password}, nil
}

const KeyAccount = `x-account`

func (s *SessionsService) VerifyNoauth(handler http.Handler) http.Handler {
	return s.verify(handler, true)
}

func (s *SessionsService) Verify(handler http.Handler) http.Handler {
	return s.verify(handler, false)
}

func (s *SessionsService) verify(handler http.Handler, noauth bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		session, err := s.store.Get(r, s.name)
		if err != nil {
			if noauth {
				handler.ServeHTTP(w, r)
				return
			}

			http.Error(w, "请先登入", 403)
			return
		}
		user, ok := session.Values[KeyAccount]
		if !ok {
			if noauth {
				handler.ServeHTTP(w, r)
				return
			}

			http.Error(w, "请先登入", 403)
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, KeyAccount, user)
		r = r.WithContext(ctx)

		handler.ServeHTTP(w, r)
	})
}

func (s *SessionsService) LoginNoauth(accountID bson.ObjectId, w http.ResponseWriter, r *http.Request) (err error) {
	user, err := s.account.Get(accountID)
	if err != nil {
		return err
	}

	session, err := s.store.Get(r, s.name)
	if err != nil {
		return err
	}

	session.Values[KeyAccount] = user
	err = s.store.Save(r, w, session)
	if err != nil {
		return err
	}
	return nil
}

func (s *SessionsService) LoginWithAccount(login *LoginWithAccount, w http.ResponseWriter, r *http.Request) (user *account.AccountWithID, err error) {
	auth, err := s.auth.GetByAccount(login.Account)
	if err != nil {
		return nil, err
	}

	err = s.LoginNoauth(auth.AccountID, w, r)
	if err != nil {
		return nil, err
	}
	return s.account.Get(auth.AccountID)
}

// Login #route:"POST /login"#
func (s *SessionsService) LoginWithPassword(login *LoginWithPassword, w http.ResponseWriter, r *http.Request) (user *account.AccountWithID, err error) {
	auth, err := s.auth.GetByAccount(login.Account)
	if err != nil {
		return nil, err
	}
	sess, err := s.password.VerifyByAccount(auth.AccountID, password.PasswordKindLogin, login.Password)
	if err != nil {
		return nil, err
	}
	if !sess {
		return nil, fmt.Errorf("用户名或者密码错误")
	}

	err = s.LoginNoauth(auth.AccountID, w, r)
	if err != nil {
		return nil, err
	}
	return s.account.Get(auth.AccountID)
}

// Logout #route:"POST /logout"#
func (s *SessionsService) Logout(w http.ResponseWriter, r *http.Request) (err error) {
	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil
	}

	session.Values = map[interface{}]interface{}{}
	err = s.store.Save(r, w, session)
	if err != nil {
		return err
	}
	return nil
}

// Check #route:"POST /check"#
func (s *SessionsService) Check(r *http.Request) (user *account.AccountWithID, err error) {
	session, err := s.store.Get(r, s.name)
	if err != nil {
		return nil, err
	}

	userI, ok := session.Values[KeyAccount]
	if !ok {
		return nil, nil
	}

	user, _ = userI.(*account.AccountWithID)
	return user, nil
}

func (s *SessionsService) GetSession(r *http.Request) (*sessions.Session, error) {
	return s.store.Get(r, s.name)

}

func (s *SessionsService) SaveSession(w http.ResponseWriter, r *http.Request, session *sessions.Session) error {
	return s.store.Save(r, w, session)

}
