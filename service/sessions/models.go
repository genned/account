package sessions

type LoginWithAccount struct {
	// 账号
	Account string `bson:"account" json:"account"`
}

type LoginWithPassword struct {
	// 账号
	Account string `bson:"account" json:"account"`
	// 密码
	Password string `bson:"password" json:"password"`
}
