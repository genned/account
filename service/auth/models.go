package auth

import (
	"github.com/globalsign/mgo/bson"
)

type Auth struct {
	AccountID bson.ObjectId `bson:"account_id" json:"account_id"`
	Auth      string        `bson:"auth" json:"auth"`
	AuthKind  AuthKind      `bson:"auth_kind" json:"auth_kind"`
}

type AuthKind string

const (
	AuthKindEmail AuthKind = "email"
	AuthKindPhone AuthKind = "phone"
)
