// Code generated; Do not regenerate the overwrite after editing.

package auth

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// AuthWithID is Auth with ID
type AuthWithID struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"auth_id"`
	Auth       `bson:",inline"`
	CreateTime time.Time `bson:"create_time,omitempty" json:"create_time"`
	UpdateTime time.Time `bson:"update_time,omitempty" json:"update_time"`
}

// AuthRecord is record of the Auth
type AuthRecord struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"auth_record_id"`
	AuthID      bson.ObjectId `bson:"auth_id,omitempty" json:"auth_id"`
	Recent      *Auth         `bson:"recent,omitempty" json:"recent"`
	Current     *Auth         `bson:"current,omitempty" json:"current"`
	RecentTime  time.Time     `bson:"recent_time,omitempty" json:"recent_time"`
	CurrentTime time.Time     `bson:"current_time,omitempty" json:"current_time"`
	Times       int           `bson:"times,omitempty" json:"times"`
}

// AuthService is service of the Auth
// #path:"/auth/"#
type AuthService struct {
	db       *mgo.Collection
	dbRecord *mgo.Collection
}

// NewAuthService Create a new AuthService
func NewAuthService(db *mgo.Collection) (*AuthService, error) {
	dbRecord := db.Database.C(db.Name + "_record")
	dbRecord.EnsureIndex(mgo.Index{Key: []string{"auth_id"}})
	return &AuthService{
		db:       db,
		dbRecord: dbRecord,
	}, nil
}

// Create a Auth
// #route:"POST /"#
func (s *AuthService) Create(auth *Auth) (authID bson.ObjectId /* #name:"auth_id"# */, err error) {
	authID = bson.NewObjectId()
	now := bson.Now()
	err = s.db.Insert(&AuthWithID{
		ID:         authID,
		Auth:       *auth,
		CreateTime: now,
		UpdateTime: now,
	})
	if err != nil {
		return "", err
	}
	return authID, nil
}

// Update the Auth
// #route:"PUT /{auth_id}"#
func (s *AuthService) Update(authID bson.ObjectId /* #name:"auth_id"# */, auth *Auth) (err error) {
	recent, err := s.Get(authID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.UpdateId(authID, bson.D{{"$set", &AuthWithID{
		Auth:       *auth,
		UpdateTime: bson.Now(),
	}}})
	if err != nil {
		return err
	}

	current, err := s.Get(authID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.record(recent, &current.Auth)
	if err != nil {
		return err
	}

	return nil
}

// Delete the Auth
// #route:"DELETE /{auth_id}"#
func (s *AuthService) Delete(authID bson.ObjectId /* #name:"auth_id"# */) (err error) {
	recent, err := s.Get(authID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.RemoveId(authID)
	if err != nil {
		return err
	}

	err = s.record(recent, nil)
	if err != nil {
		return err
	}

	return nil
}

// Get the Auth
// #route:"GET /{auth_id}"#
func (s *AuthService) Get(authID bson.ObjectId /* #name:"auth_id"# */) (auth *AuthWithID, err error) {
	q := s.db.FindId(authID)
	err = q.One(&auth)
	if err != nil {
		return nil, err
	}
	return auth, nil
}

// GetByAccount the Auth
// #route:"GET /account/{account}"#
func (s *AuthService) GetByAccount(account string /* #name:"account"# */) (auth *AuthWithID, err error) {
	q := s.db.Find(bson.D{{"auth", account}})
	err = q.One(&auth)
	if err != nil {
		return nil, err
	}
	return auth, nil
}

// List of the Auth
// #route:"GET /"#
func (s *AuthService) List(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */, offset, limit int, sort string) (auths []*AuthWithID, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m).Skip(offset).Limit(limit).Sort(sort)
	err = q.All(&auths)
	if err != nil {
		return nil, err
	}
	return auths, nil
}

// Count of the Auth
// #route:"GET /count"#
func (s *AuthService) Count(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */) (count int, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m)
	return q.Count()
}

// RecordList of the Auth record list
// #route:"GET /{auth_id}/record"#
func (s *AuthService) RecordList(authID bson.ObjectId /* #name:"auth_id"# */, offset, limit int) (authRecords []*AuthRecord, err error) {
	m := bson.D{{"auth_id", authID}}
	q := s.dbRecord.Find(m).Skip(offset).Limit(limit)
	err = q.All(&authRecords)
	if err != nil {
		return nil, err
	}
	return authRecords, nil
}

// RecordCount of the Auth record count
// #route:"GET /{auth_id}/record/count"#
func (s *AuthService) RecordCount(authID bson.ObjectId /* #name:"auth_id"# */) (count int, err error) {
	m := bson.D{{"auth_id", authID}}
	q := s.dbRecord.Find(m)
	return q.Count()
}

func (s *AuthService) record(auth *AuthWithID, current *Auth) error {
	if auth == nil {
		return nil
	}
	count, err := s.dbRecord.Find(bson.D{{"auth_id", auth.ID}}).Count()
	if err != nil {
		return err
	}
	record := &AuthRecord{
		AuthID:      auth.ID,
		Current:     current,
		CurrentTime: bson.Now(),
		Times:       count + 1,
		Recent:      &auth.Auth,
		RecentTime:  auth.UpdateTime,
	}
	return s.dbRecord.Insert(record)
}
