// Code generated; Do not regenerate the overwrite after editing.

package password

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	pwd "github.com/wzshiming/password"
)

// PasswordWithID is Password with ID
type PasswordWithID struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"password_id"`
	Password   `bson:",inline"`
	CreateTime time.Time `bson:"create_time,omitempty" json:"create_time"`
	UpdateTime time.Time `bson:"update_time,omitempty" json:"update_time"`
}

// PasswordRecord is record of the Password
type PasswordRecord struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"password_record_id"`
	PasswordID  bson.ObjectId `bson:"password_id,omitempty" json:"password_id"`
	Recent      *Password     `bson:"recent,omitempty" json:"recent"`
	Current     *Password     `bson:"current,omitempty" json:"current"`
	RecentTime  time.Time     `bson:"recent_time,omitempty" json:"recent_time"`
	CurrentTime time.Time     `bson:"current_time,omitempty" json:"current_time"`
	Times       int           `bson:"times,omitempty" json:"times"`
}

// PasswordService is service of the Password
// #path:"/password/"#
type PasswordService struct {
	db       *mgo.Collection
	dbRecord *mgo.Collection
}

// NewPasswordService Create a new PasswordService
func NewPasswordService(db *mgo.Collection) (*PasswordService, error) {
	dbRecord := db.Database.C(db.Name + "_record")
	dbRecord.EnsureIndex(mgo.Index{Key: []string{"password_id"}})
	return &PasswordService{
		db:       db,
		dbRecord: dbRecord,
	}, nil
}

// VerifyByAccount #route:"POST /{password_kind}/{account_id}"#
func (s *PasswordService) VerifyByAccount(accountID bson.ObjectId /* #name:"account_id"# */, passwordKind PasswordKind /* #name:"password_kind"# */, password string /* #in:"body"# */) (b bool, err error) {
	pass, err := s.GetByAccount(accountID, passwordKind)
	if err != nil {
		return false, err
	}
	return pwd.Verify(pass.ID.Hex()+password, pass.Code), nil
}

// Verify #route:"POST /verify"#
func (s *PasswordService) Verify(passwordID bson.ObjectId /* #name:"password_id"# */, password string /* #in:"body"# */) (b bool, err error) {
	pass, err := s.Get(passwordID)
	if err != nil {
		return false, err
	}

	return pwd.Verify(passwordID.Hex()+password, pass.Code), nil
}

// Create a Password
// #route:"POST /"#
func (s *PasswordService) Create(password *Password) (passwordID bson.ObjectId /* #name:"password_id"# */, err error) {
	passwordID = bson.NewObjectId()
	now := bson.Now()
	password.Code = pwd.Encrypt(passwordID.Hex() + password.Code)
	err = s.db.Insert(&PasswordWithID{
		ID:         passwordID,
		Password:   *password,
		CreateTime: now,
		UpdateTime: now,
	})
	if err != nil {
		return "", err
	}
	return passwordID, nil
}

// UpdateByAccount #route:"PUT /{password_kind}/{account_id}"#
func (s *PasswordService) UpdateByAccount(accountID bson.ObjectId /* #name:"account_id"# */, passwordKind PasswordKind /* #name:"password_kind"# */, password string /* #in:"body"# */) (err error) {
	pass, err := s.GetByAccount(accountID, passwordKind)
	if err != nil {
		return err
	}
	return s.Update(pass.ID, &Password{
		AccountID:    accountID,
		PasswordKind: passwordKind,
		Code:         password,
	})
}

// Update the Password
// #route:"PUT /{password_id}"#
func (s *PasswordService) Update(passwordID bson.ObjectId /* #name:"password_id"# */, password *Password) (err error) {
	recent, err := s.Get(passwordID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	password.Code = pwd.Encrypt(passwordID.Hex() + password.Code)

	err = s.db.UpdateId(passwordID, bson.D{{"$set", &PasswordWithID{
		Password:   *password,
		UpdateTime: bson.Now(),
	}}})
	if err != nil {
		return err
	}

	current, err := s.Get(passwordID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.record(recent, &current.Password)
	if err != nil {
		return err
	}

	return nil
}

// Delete the Password
// #route:"DELETE /{password_id}"#
func (s *PasswordService) Delete(passwordID bson.ObjectId /* #name:"password_id"# */) (err error) {
	recent, err := s.Get(passwordID)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return err
	}

	err = s.db.RemoveId(passwordID)
	if err != nil {
		return err
	}

	err = s.record(recent, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetByAccount #route:"GET /{password_kind}/{account_id}"#
func (s *PasswordService) GetByAccount(accountID bson.ObjectId /* #name:"account_id"# */, passwordKind PasswordKind /* #name:"password_kind"# */) (password *PasswordWithID, err error) {
	q := s.db.Find(bson.D{{"account_id", accountID}, {"password_kind", passwordKind}})
	err = q.One(&password)
	if err != nil {
		return nil, err
	}
	return password, nil
}

// Get the Password
// #route:"GET /{password_id}"#
func (s *PasswordService) Get(passwordID bson.ObjectId /* #name:"password_id"# */) (password *PasswordWithID, err error) {
	q := s.db.FindId(passwordID)
	err = q.One(&password)
	if err != nil {
		return nil, err
	}
	return password, nil
}

// List of the Password
// #route:"GET /"#
func (s *PasswordService) List(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */, offset, limit int, sort string) (passwords []*PasswordWithID, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m).Skip(offset).Limit(limit).Sort(sort)
	err = q.All(&passwords)
	if err != nil {
		return nil, err
	}
	return passwords, nil
}

// Count of the Password
// #route:"GET /count"#
func (s *PasswordService) Count(startTime /* #name:"start_time"# */, endTime time.Time /* #name:"end_time"# */) (count int, err error) {
	m := bson.D{}
	if !startTime.IsZero() || !endTime.IsZero() {
		m0 := bson.D{}
		if !startTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$gte", startTime})
		}
		if !endTime.IsZero() {
			m0 = append(m0, bson.DocElem{"$lt", endTime})
		}
		m = append(m, bson.DocElem{"create_time", m0})
	}
	q := s.db.Find(m)
	return q.Count()
}

// RecordList of the Password record list
// #route:"GET /{password_id}/record"#
func (s *PasswordService) RecordList(passwordID bson.ObjectId /* #name:"password_id"# */, offset, limit int) (passwordRecords []*PasswordRecord, err error) {
	m := bson.D{{"password_id", passwordID}}
	q := s.dbRecord.Find(m).Skip(offset).Limit(limit)
	err = q.All(&passwordRecords)
	if err != nil {
		return nil, err
	}
	return passwordRecords, nil
}

// RecordCount of the Password record count
// #route:"GET /{password_id}/record/count"#
func (s *PasswordService) RecordCount(passwordID bson.ObjectId /* #name:"password_id"# */) (count int, err error) {
	m := bson.D{{"password_id", passwordID}}
	q := s.dbRecord.Find(m)
	return q.Count()
}

func (s *PasswordService) record(password *PasswordWithID, current *Password) error {
	if password == nil {
		return nil
	}
	count, err := s.dbRecord.Find(bson.D{{"password_id", password.ID}}).Count()
	if err != nil {
		return err
	}
	record := &PasswordRecord{
		PasswordID:  password.ID,
		Current:     current,
		CurrentTime: bson.Now(),
		Times:       count + 1,
		Recent:      &password.Password,
		RecentTime:  password.UpdateTime,
	}
	return s.dbRecord.Insert(record)
}
