package password

import (
	"github.com/globalsign/mgo/bson"
)

type Password struct {
	AccountID bson.ObjectId `bson:"account_id" json:"account_id"`
	// After the password is encrypted
	Code         string       `bson:"code" json:"code"`
	PasswordKind PasswordKind `bson:"password_kind" json:"password_kind"`
}

type PasswordKind string

const (
	PasswordKindLogin PasswordKind = "login"
	PasswordKindPay   PasswordKind = "pay"
)
